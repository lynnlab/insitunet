#!/bin/env python3

"""
Mass Spectrometry Imaging (MSI) data importer for InsituNet.

This script processes MSI data in csv form, as exported from SCiLS Lab and
converts it to an InsituNet formated csv.

If multiple files are imported with misaligned spectra, binning will be 
performed to enable simultaneous comparison within InsituNet.
Bin width can be optionally specified with the -b switch.

As fundamentally the MSI data include intensity on every possible grid space,
this script also thesholds this intensity at a specified quantile.
Quantile threshold can be optionally specified with the -q switch.

Example usage:

    ./msi_importer.py -i msi_data_1.csv msi_data_2.csv -o outdir/
    
Converted csv files will be appended with "for_insitunet.csv".
The result can then be directly opened and analysed in InsituNet using the
same procedures as for spatially resolved transcriptomics.
    
"""
# Import libraries
from argparse import ArgumentParser
import os
import numpy as np
import pandas as pd

# Process cli arguments
parser = ArgumentParser()
parser.add_argument("-i", "--input", nargs='+', dest="input", required=True,
                    help="MSI csv file(s) to convert", metavar="INPUT_CSV")
parser.add_argument("-o", "--outdir", dest="outdir", default='.',
                    help="where to save converted files",
                    metavar="OUTPUT_DIRECTORY")
parser.add_argument("-b", "--binwidth", dest="binwidth", default=0.009661,
                    help="bin width in microns", metavar="FLOAT")
parser.add_argument("-q", "--quantile", dest="quantile", default=0.95,
                    help="quantile threshold for export", metavar="FLOAT")
args = parser.parse_args()

# Import each csv specified
input_data = {} 
for csv in args.input:
    ms = pd.read_csv(csv, index_col=0)
    positions = ms[['X', 'Y']]
    data = ms.iloc[:,2:-1]
    input_data[csv] = (positions, data)

# Pre-sort
sorted_data = {}
xmi = []
xma = []
for name, (positions, data) in input_data.items():
    s = data[data.columns.sort_values()]
    s.columns = s.columns.astype('float')
    xmi.append(s.columns.min())
    xma.append(s.columns.max())
    sorted_data[name] = s
    
# Perform binning
bin_width = float(args.binwidth)
binned_data = {}
bins = np.arange(np.floor(np.min(xmi)), np.ceil(np.max(xma)), bin_width)
column_names = []
# iterate sorted data
for name, data in sorted_data.items():
    frame = pd.DataFrame(index=data.index)
    # get bin index of each column
    inds = np.digitize(data.columns.values, bins)
    # iterate bins by index
    for i in inds:
        bin_center = bins[i] - ((bins[i] - bins[i-1])/2)
        # get all columns belonging to given bin, and sum them along rows
        d = data.loc[:,inds == i].sum(axis=1)
        if d.sum() != 0:
            frame[bin_center] = d
    binned_data[name] = frame
    column_names.append(frame.columns)

# test how many overlapping masses we have by this binning
overlap = set.intersection(*[set(x) for x in column_names])
print(f'Found {len(overlap)} cross-file peaks with bin width of {bin_width}')

# Export with quantile thresholding
q = float(args.quantile)
q_value = pd.Series(data.values.flatten()).quantile(q)
for name, (positions, data) in input_data.items():
    export = []
    for mass in data:
        pos = positions[data[mass] > q_value]
        if len(pos > 0):
            for _, point in pos.iterrows():
                export.append([mass, point.X, point.Y])
    export_fn = os.path.basename(name) + '_for_insitunet.csv'
    pd.DataFrame(export, columns=['name', 'x_pos', 'y_pos'])\
        .to_csv(args.outdir + '/' + export_fn, index=False)
    print(f'Successfully created {export_fn}')
