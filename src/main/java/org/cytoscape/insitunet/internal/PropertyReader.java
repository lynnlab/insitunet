package org.cytoscape.insitunet.internal;

import org.cytoscape.property.AbstractConfigDirPropsReader;
import org.cytoscape.property.CyProperty;

public class PropertyReader extends AbstractConfigDirPropsReader {

	public PropertyReader(String name, String fileName) {
		super(name, fileName, CyProperty.SavePolicy.CONFIG_DIR);
	}
}
