package org.cytoscape.insitunet.internal;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.cytoscape.insitunet.internal.typenetwork.Transcript;

public class Gene implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1218671908909299115L;
	String name;
	final List<Transcript> transcripts = new ArrayList<>();
	float[] positionArray; // OpenGL usable array
	Color color;
	Symbol symbol;
	Integer index;
	Float symbolScale = 1f;
	boolean enabled = false;

	public Gene(String name, Integer index) {
		this.name = name;
		this.index = index;
	}

	public void setScale(float f) {
		symbolScale = f;
	}

	public float getScale() {
		return symbolScale;
	}

	public void setColor(Color c) {
		this.color = c;
	}

	public void setSymbol(Symbol s) {
		symbol = s;
	}

	public Symbol getSymbol() {
		return symbol;
	}

	void setIndex(Integer i) {
		index = i;
	}

	public Integer getIndex() {
		return index;
	}

	public void addTranscript(Transcript t) {
		transcripts.add(t);
	}

	public String getName() {
		return name;
	}

	public void generateArrays() {
		positionArray = new float[transcripts.size() * 2];
		transcriptsToArray(transcripts, positionArray);
	}

	public float[] getArray() {
		return positionArray;
	}

	public float[] getColorRGB() {
		return color.getRGBColorComponents(null);
	}

	public Color getColor() {
		return color;
	}

	public List<Transcript> getTranscripts() {
		return transcripts;
	}

	public static void transcriptsToArray(List<Transcript> list, float[] array) {
		int i = 0;
		for (Transcript t : list) {
			array[i++] = (float) t.getX();
			array[i++] = (float) t.getY();
		}
	}

	@Override
	public boolean equals(Object other) {
		Gene oG = (Gene) other;
		return this.getName().equals(oG.getName());
	}

	@Override
	public String toString() {
		return name;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean b) {
		enabled = b;
	}
}