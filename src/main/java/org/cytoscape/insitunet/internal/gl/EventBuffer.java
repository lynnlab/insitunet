package org.cytoscape.insitunet.internal.gl;

import java.nio.FloatBuffer;
import java.util.ArrayDeque;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;

/**
 * Animating all the time wastes CPU, but only animating once per input event
 * results in "laggy" behaviour. (Because fire faster than maximum refresh rate)
 * This class remedies this by acting as a buffer zone between input events and
 * animation.
 * 
 * @author John Salamon
 *
 */
class EventBuffer {

	static final float EVENT_THROTTLE = 0.6f;
	static final int ZOOM_AMPLIFIER = 8;

	// The event queue
	final ArrayDeque<Event> eventQueue = new ArrayDeque<>();
	final Animator animator;
	final EventHandler handler;

	String outputPath = null;

	public EventBuffer(GLAutoDrawable drawable, EventHandler handler) {
		this.animator = new Animator(drawable);
		this.handler = handler;
		animator.start();
	}

	/**
	 * Sometimes you need things to just go.
	 */
	public void justGo() {
		eventQueue.add(new Event(EventType.GENERIC));
		animator.go();
	}

	public void add(Event e) {
		/*
		 * if(e.type == EventType.ZOOM) { // zoom event for(int i = 0; i <
		 * ZOOM_AMPLIFIER; i++) { eventQueue.add(e); } } else {
		 */
		eventQueue.add(e);
		// }
		animator.go();
	}

	/**
	 * Apply events as needed. Returns false if none.
	 */
	public boolean runEvents(GL3 gl) {

		if (eventQueue.isEmpty())
			return false;

		Event zoom = null;
		Event legend = null;


		while (eventQueue.size() > 0) {
			try {
				Event e = eventQueue.remove();
				handler.apply(gl, e);
				if (e.type == EventType.ZOOM) {
					float factor = (float) e.modifier;
					float newf = factor;
					if (factor < 1.0f)
						newf = 1 / newf;
					newf = newf - 1;
					newf = newf / 1.8f;
					newf = newf + 1;
					if (newf < 1.00001)
						continue;
					if (factor < 1.0f)
						newf = 1 / newf;
					zoom = new Event(e.x, e.y, newf, EventType.ZOOM);
				}
				if (e.type == EventType.LEGEND) {
					float factor = (float) e.modifier;
					factor /= 2f;
					if (Math.abs(factor) < 0.01)
						continue;
					legend = new Event(e.x, e.y, factor, EventType.LEGEND);
				}

			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}

		if (zoom != null) {
			eventQueue.add(zoom);
			animator.go();
		}
		if (legend != null) {
			eventQueue.add(legend);
			animator.go();
		}

		return true;
	}

	public enum EventType {
		ZOOM, PAN, ROTATE, SELECTION, CENTER, RESHAPE, IMAGE, GENERIC, LEGEND
	}

	public static class Event {

		final float x;
		final float y;
		final Object modifier;
		final EventType type;

		public Event(float x, float y, Object modifier, EventType type) {
			this.x = x;
			this.y = y;
			this.modifier = modifier;
			this.type = type;
		}

		public Event(float x, float y) {
			this.x = x;
			this.y = y;
			this.modifier = 0;
			this.type = EventType.PAN;
		}

		class ShapeData {
			FloatBuffer shape;
			boolean complete;

			ShapeData(FloatBuffer s, boolean b) {
				shape = s;
				complete = b;
			}
		}

		public Event(FloatBuffer shape, boolean complete) {
			this.x = 0;
			this.y = 0;
			this.modifier = new ShapeData(shape, complete);
			this.type = EventType.SELECTION;
		}

		public Event(EventType generic) {
			this.x = 0;
			this.y = 0;
			this.modifier = 0.0;
			this.type = EventType.GENERIC;
		}
	}

	public interface EventHandler {

		public void apply(GL3 gl, Event e);

	}

	public void shutDown() {
		this.animator.stop();
	}

}