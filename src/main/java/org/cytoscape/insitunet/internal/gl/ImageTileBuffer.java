package org.cytoscape.insitunet.internal.gl;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.GLBuffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

/**
 * Attempts to create an image that will back the points. If the required image
 * is larger than MAX_TEXTURE_SIZE, we can split it into up to MAX_TEXTURE_UNITS
 * tiles. If it's still too large, we have to scale it down.
 */
public class ImageTileBuffer implements VertexBuffer {
	// TODO: Have scale-down option
	FloatBuffer texCoordBuffer, tileGeometryBuffer;

	final Integer pointVBO, texcoordVBO;
	final Integer imageVAO;
	final int elementSize = GLBuffers.SIZEOF_FLOAT;
	final List<Texture> tileTextures = new ArrayList<>();
	final int MAX_TEXTURE_SIZE, MAX_TEXTURE_UNITS;
	final ProgramData program;
	final Texture zeroTexture = TextRenderer.zeroTexture();

	float imageScale = 1;

	public void setImageScale(float scale) {
		imageScale = scale;
	}

	public float getImageScale() {
		return imageScale;
	}

	public ImageTileBuffer(GL3 gl, ProgramData program) {

		int vbo[] = new int[2];
		gl.glGenBuffers(2, vbo, 0);
		this.pointVBO = vbo[0];
		this.texcoordVBO = vbo[1];
		this.program = program;

		IntBuffer vao = GLBuffers.newDirectIntBuffer(1);
		gl.glGenVertexArrays(1, vao);
		imageVAO = vao.get(0);

		int[] b0 = new int[1];
		int[] b1 = new int[1];
		gl.glGetIntegerv(GL3.GL_MAX_TEXTURE_SIZE, IntBuffer.wrap(b0));
		MAX_TEXTURE_SIZE = b0[0]; // yea boi
		gl.glGetIntegerv(GL3.GL_MAX_TEXTURE_IMAGE_UNITS, IntBuffer.wrap(b1));
		MAX_TEXTURE_UNITS = b1[0];

		genVertexArray(gl);
	}

	public void setImage(GL3 gl, BufferedImage image) {
		gl.glUseProgram(program.getProgram());

		Dimension reqiuredTiles = getRequiredTiles(image);
		bindTextureData(gl, reqiuredTiles, image);
		generateBuffers(reqiuredTiles, image);
		bufferData(gl);
		System.out.println("TileBuffer has loaded " + tileTextures.size() + " tile(s)");

		gl.glUseProgram(0);
	}

	private Dimension getRequiredTiles(BufferedImage bufferedImage) {

		// Will describe how many tiles are needed in each dimension.
		Dimension tiles = new Dimension();

		double width = bufferedImage.getWidth() / (double) MAX_TEXTURE_SIZE;
		if (width <= 1d) {
			tiles.width = 1;
		} else {
			tiles.width = (int) Math.ceil(width);
		}
		double height = bufferedImage.getHeight() / (double) MAX_TEXTURE_SIZE;
		if (height <= 1d) {
			tiles.height = 1;
		} else {
			tiles.height = (int) Math.ceil(height);
		}

		int num = tiles.width * tiles.height;
		if (num > MAX_TEXTURE_UNITS) {
			System.out.println("Required " + num + " tiles, but only " + MAX_TEXTURE_UNITS + " texture units.");
		}
		return tiles;
	}

	public void bindTextureData(GL3 gl, Dimension reqiuredTiles, BufferedImage bufferedImage) {

		int tilew = bufferedImage.getWidth() / reqiuredTiles.width;
		int tileh = bufferedImage.getHeight() / reqiuredTiles.height;
		BufferedImage sub = new BufferedImage(tilew, tileh, BufferedImage.TYPE_INT_ARGB);
		tileTextures.clear();

		for (int i = 0; i < reqiuredTiles.width * reqiuredTiles.height && i < MAX_TEXTURE_UNITS - 1; i++) {

			int vl1 = (int) ((i % reqiuredTiles.width) * tilew);
			int vu1 = (int) (((i / reqiuredTiles.width) % reqiuredTiles.height) * tileh);

			Graphics2D g = (Graphics2D) sub.getGraphics();
			g.drawImage(bufferedImage, 0, 0, tilew, tileh, vl1, vu1, vl1 + tilew, vu1 + tileh, null);
			g.dispose();

			Texture tile = AWTTextureIO.newTexture(GLProfile.get(GLProfile.GL3), sub, true);
			tile.setTexParameteri(gl, GL3.GL_TEXTURE_MIN_FILTER, GL3.GL_LINEAR_MIPMAP_LINEAR);
			tile.setTexParameteri(gl, GL3.GL_TEXTURE_MAG_FILTER, GL3.GL_LINEAR);
			tile.setTexParameteri(gl, GL3.GL_TEXTURE_WRAP_S, GL3.GL_CLAMP_TO_EDGE);
			tile.setTexParameteri(gl, GL3.GL_TEXTURE_WRAP_T, GL3.GL_CLAMP_TO_EDGE);

			gl.glGenerateMipmap(GL3.GL_TEXTURE_2D);

			tileTextures.add(tile);
		}

		for (int i = 0; i < tileTextures.size(); i++) {
			gl.glActiveTexture(GL3.GL_TEXTURE0 + i + 1);
			gl.glBindTexture(GL3.GL_TEXTURE_2D, tileTextures.get(i).getTextureObject());
		}
		gl.glActiveTexture(GL3.GL_TEXTURE0);
		zeroTexture.bind(gl); // done, bind the zero texture
	}

	public void bufferData(GL3 gl) {

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, pointVBO);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, tileGeometryBuffer.capacity() * elementSize, tileGeometryBuffer,
				GL3.GL_STATIC_DRAW);

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, texcoordVBO);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, texCoordBuffer.capacity() * elementSize, texCoordBuffer,
				GL3.GL_STATIC_DRAW);

	}

	private void generateBuffers(Dimension reqiuredTiles, BufferedImage bufferedImage) {

		float tileWidth = (float) Math.ceil((double) bufferedImage.getWidth() / reqiuredTiles.width);
		float tileHeight = (float) Math.ceil((double) bufferedImage.getHeight() / reqiuredTiles.height);

		texCoordBuffer = FloatBuffer.allocate(12 * tileTextures.size());
		tileGeometryBuffer = FloatBuffer.allocate(12 * tileTextures.size());
		float[] texcoordQuad = VertexBuffer.makeQuad(0, 0, 1, 1);
		int x = 0;
		for (int i = 0; i < tileTextures.size(); i++) {

			float x1 = (i % reqiuredTiles.width) * tileWidth;
			float y1 = ((i / reqiuredTiles.width) % reqiuredTiles.height) * tileHeight;
			float x2 = (float) x1 + tileWidth;
			float y2 = (float) y1 + tileHeight;

			float[] geometryQuad = VertexBuffer.makeQuad(x1, y1, x2, y2);
			System.out.println(String.format("%f, %f, %f,%f", x1, y1, x2, y2));
			for (int j = 0; j < geometryQuad.length; j++) {
				tileGeometryBuffer.put(x, geometryQuad[j]);
				texCoordBuffer.put(x, texcoordQuad[j]);
				x++;
			}
		}
	}

	@Override
	public void genVertexArray(GL3 gl) {
		int stride = 0;
		int offset = 0;

		gl.glBindVertexArray(imageVAO);
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, texcoordVBO);
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, pointVBO);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);

	}

	@Override
	public void render(GL3 gl) {

		gl.glUseProgram(program.getProgram());
		Matrix customScale = new Matrix(program.getScale()).scale(imageScale);
		Matrix alteredScale = new Matrix().multiply(program.getTranslation()).multiply(customScale)
				.multiply(program.getRotation());

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, alteredScale.getBuffer()); // reset the modelview matrix

		gl.glBindVertexArray(imageVAO);
		{
			gl.glUniform1f(program.getPointScale(), 0f);
			gl.glUniform4f(program.getColour(), 0, 0, 0, 0); // bind the gene colour
			for (int i = 0; i < tileTextures.size(); i++) {
				gl.glActiveTexture(GL3.GL_TEXTURE0 + i + 1);
				gl.glUniform1i(program.getSampler(), i + 1);
				gl.glDrawArrays(GL3.GL_TRIANGLES, i * 6, 6);
			}
			gl.glUniform1i(program.getSampler(), 0);
			gl.glActiveTexture(GL3.GL_TEXTURE0);
		}
		gl.glBindVertexArray(0);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer()); // reset the
																									// modelview matrix
		gl.glUseProgram(0);

	}

}
