package org.cytoscape.insitunet.internal.gl;

import java.nio.FloatBuffer;

/**
 * A minimal 4x4 matrix class for graphical applications. Operations assume
 * layout is in in column major.
 * 
 * @author John Salamon
 *
 */
public class Matrix {

	// For easier indexing of column major layout.
	public static Integer m00 = 0, m10 = 4, m20 = 8, m30 = 12, m01 = 1, m11 = 5, m21 = 9, m31 = 13, m02 = 2, m12 = 6,
			m22 = 10, m32 = 14, m03 = 3, m13 = 7, m23 = 11, m33 = 15;

	final FloatBuffer buffer = FloatBuffer.allocate(16);

	public static float[] identity = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, };

	public Matrix() {
		identity();
	}

	public Matrix(Matrix m) {
		copy(m);
	}

	/**
	 * Copies the given matrix into this one.
	 * 
	 * @param m
	 *            The matrix to be copied.
	 */
	public Matrix copy(Matrix m) {
		buffer.rewind();
		buffer.put(m.getBuffer().array());
		return this;
	}

	public FloatBuffer getBuffer() {
		buffer.rewind();
		return buffer;
	}

	public float get(int i) {
		return buffer.get(i);
	}

	public void put(int i, float f) {
		buffer.put(i, f);
	}

	/**
	 * Multiply this matrix by another matrix.
	 */
	public Matrix multiply(Matrix r) {

		for (int row = 0; row < 4; row++) {
			// get row from left matrix
			float a = get(0 * 4 + row);
			float b = get(1 * 4 + row);
			float c = get(2 * 4 + row);
			float d = get(3 * 4 + row);

			// get every column in turn from right matrix
			for (int col = 0; col < 4; col++) {
				float x = r.get(0 + col * 4);
				float y = r.get(1 + col * 4);
				float z = r.get(2 + col * 4);
				float w = r.get(3 + col * 4);

				put(col * 4 + row, a * x + b * y + c * z + d * w);
			}
		}
		return this;
	}

	/**
	 * Multiply matrix by vector, returns new vector.
	 * 
	 * @param v
	 * @return
	 */
	public float[] multiply(float[] v) {

		float[] result = new float[4];
		for (int i = 0; i < 4; i++) {
			// get row from left matrix
			float a = get(0 * 4 + i);
			float b = get(1 * 4 + i);
			float c = get(2 * 4 + i);
			float d = get(3 * 4 + i);
			// get every column in turn from right matrix

			result[i] = a * v[0] + b * v[1] + c * v[2] + d * v[3];
		}
		return result;
	}

	public Matrix ortho(float left, float right, float bottom, float top, float z_near, float z_far) {

		float a = 2f / (right - left);
		float b = 2f / (top - bottom);
		float c = -2f / (z_far - z_near);
		float r = -(right + left) / (right - left);
		float s = -(top + bottom) / (top - bottom);
		float t = -(z_far + z_near) / (z_far - z_near);

		identity();
		put(m00, a);
		put(m11, b);
		put(m22, c);
		put(m30, r);
		put(m31, s);
		put(m32, t);
		put(m33, 1);

		return this;
	}

	public Matrix identity() {

		buffer.rewind();

		buffer.put(identity);
		return this;
	}

	/**
	 * Scales by constant factor in 3 dimensions.
	 * 
	 * @param scale
	 * @return
	 */
	public Matrix scale(float scale) {

		put(m00, get(m00) * scale);
		put(m11, get(m11) * scale);
		put(m22, get(m22) * scale);

		return this;
	}

	/**
	 * Will produce the inverse matrix if this is a scale matrix.
	 */
	public Matrix scaleInverse() {

		Matrix inv = new Matrix();
		inv.put(m00, 1f / get(m00));
		inv.put(m11, 1f / get(m11));
		inv.put(m22, 1f / get(m22));

		return inv;
	}

	/**
	 * Create a rotation around the Z axis.
	 */
	public Matrix rotateZ(float angle) {

		Matrix r = new Matrix();
		r.put(m00, (float) Math.cos(angle));
		r.put(m10, (float) -Math.sin(angle));
		r.put(m01, (float) Math.sin(angle));
		r.put(m11, (float) Math.cos(angle));
		this.multiply(r);

		return this;
	}

	/**
	 * Return the transpose of this matrix. Does not apply it to this one.
	 */
	public Matrix transpose() {

		Matrix t = new Matrix();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				t.put(j * 4 + i, get(i * 4 + j));
			}
		}
		return t;
	}

	/**
	 * If you give it an array too big that's your problem
	 * 
	 * @param vec
	 * @return
	 */
	public Matrix translate(float x, float y, float z) {

		for (int row = 0; row < 4; row++) {
			// get row from left matrix
			float a = buffer.get(0 * 4 + row);
			float b = buffer.get(1 * 4 + row);
			float c = buffer.get(2 * 4 + row);
			float d = buffer.get(3 * 4 + row);

			put(3 * 4 + row, a * x + b * y + c * z + d);
		}

		return this;
	}

	/**
	 * Translation-only matrix translator.
	 */
	public Matrix translateBasic(float x, float y, float z) {
		put(m30, x + get(m30));
		put(m31, y + get(m31));
		put(m32, z + get(m32));
		return this;
	}

	/**
	 * Will produce the inverse matrix if this is a translation matrix.
	 */
	public Matrix translateInverse() {

		Matrix inv = new Matrix();
		inv.put(m30, -get(m30));
		inv.put(m31, -get(m31));
		inv.put(m32, -get(m32));

		return inv;
	}

	/**
	 * Pretty-print a matrix.
	 */
	public static void printMatrix(Matrix m) {
		for (int i = 0; i < 4; i++) {
			System.out.println(
					m.get(0 * 4 + i) + "," + m.get(1 * 4 + i) + "," + m.get(2 * 4 + i) + "," + m.get(3 * 4 + i));
		}
	}

}
