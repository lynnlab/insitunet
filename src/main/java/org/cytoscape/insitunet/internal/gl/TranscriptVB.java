package org.cytoscape.insitunet.internal.gl;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.Symbol;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.util.GLBuffers;

public class TranscriptVB implements VertexBuffer {

	final Integer point_vbo;
	final IntBuffer vao = GLBuffers.newDirectIntBuffer(1);
	public int elementSize = GLBuffers.SIZEOF_FLOAT;
	public int usage = GL3.GL_STATIC_DRAW;

	final Buffer pointBuffer;

	final ProgramData program;
	final Gene gene;

	// TODO: Want a masterScale float somewhere
	public TranscriptVB(GL3 gl, Gene g, ProgramData program) {
		this.program = program;
		int vbo[] = new int[1];
		gl.glGenBuffers(1, vbo, 0);
		this.point_vbo = vbo[0];
		this.pointBuffer = FloatBuffer.wrap(g.getArray());
		this.gene = g;
		bufferData(gl);
		genVertexArray(gl);
	}

	public void bufferData(GL3 gl) {
		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, point_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, pointBuffer.capacity() * elementSize, pointBuffer, usage);
	}

	public void genVertexArray(GL3 gl) {
		gl.glGenVertexArrays(1, vao);
		gl.glBindVertexArray(vao.get(0));
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, program.getSymbolBuffer());
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, 0, 0);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, point_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, 0, 0);
				gl.glVertexAttribDivisor(1, 1);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);
	}

	public void render(GL3 gl) {
		if (!program.getShowAll() && !gene.isEnabled())
			return;
		gl.glUseProgram(program.getProgram());
		gl.glBindVertexArray(vao.get(0));

		float[] c = gene.getColorRGB();
		gl.glUniform4f(program.getColour(), c[0], c[1], c[2], 1);
		gl.glUniform1f(program.getPointScale(), gene.getScale() * program.getSymbolMasterScale());

		/*
		 * NOTE: From my tests at least, when using 1e6+ points, GL_TRIANGLES performs
		 * significantly better than GL_LINE_LOOP.
		 */
		Symbol s = gene.getSymbol();
		gl.glDrawArraysInstanced(s.getEnum(), gene.getSymbol().getOffset() / 2, s.getLength() / 2,
				pointBuffer.capacity() / 2);

		gl.glBindVertexArray(0);
		gl.glUseProgram(0);
	}
}