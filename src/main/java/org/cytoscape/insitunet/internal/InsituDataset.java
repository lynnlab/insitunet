package org.cytoscape.insitunet.internal;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.insitunet.internal.gl.Point2D;
import org.cytoscape.insitunet.internal.gl.Shape2D;
import org.cytoscape.insitunet.internal.panel.ControlSet;
import org.cytoscape.insitunet.internal.panel.SelectionPanel;
import org.cytoscape.insitunet.internal.typenetwork.FilteredNetwork;
import org.cytoscape.insitunet.internal.typenetwork.ListedNetwork;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.vizmap.VisualStyle;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeySizeException;

/**
 * Encapsulates all information required for a single dataset. Notably: A
 * SelectionPanel, which houses the OpenGL viewing area,
 * 
 * @author John Salamon
 */

public class InsituDataset implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3571082119667623201L;
	final KDTree<Transcript> tree;
	final String filename;
	final Rectangle2D.Double rectangle;

	final List<Gene> genes;
	final Map<String, Integer> quickIndex;
	final Integer totalSize;

	// Unfiltered network generated for this dataset at the distance specified in
	// searchDistance
	final FilteredNetwork filteredNetwork;

	transient InsituNetActivator activator;

	ControlSet controls = new ControlSet();
	transient SelectionPanel panel;

	public InsituDataset(InsituNetActivator activator, String filename, List<Gene> genes, KDTree<Transcript> tree,
			Integer index) throws InterruptedException

	{
		this.filename = filename;
		this.tree = tree;
		this.genes = genes;
		this.quickIndex = new HashMap<>();
		for (Gene g : genes) {
			quickIndex.put(g.toString(), genes.indexOf(g));
		}
		this.totalSize = index;
		Point2D minSize = getMinSize();
		Point2D maxSize = getMaxSize();
		this.rectangle = new Rectangle2D.Double(minSize.x, minSize.y, maxSize.x - minSize.x, maxSize.y - minSize.y);
		this.activator = activator;
		initGeneList();

		filteredNetwork = new FilteredNetwork(this); // Contains coexpression matrix

		panel = new SelectionPanel(this, activator);
	}

	public InsituNetActivator getActivator() {
		return activator;
	}

	public void updateGeneStyle(Gene gene) {
		filteredNetwork.updateColour(gene);
	}

	public void updateNetwork(CySwingAppAdapter adapter, CyLayoutAlgorithm algorithm) throws InterruptedException {
		controls.algorithm = algorithm;
		filteredNetwork.doSearch(tree, adapter, controls, Arrays.asList(panel.getGLPanel().getShape()));
	}

	/**
	 * Autoslider method, gets given a list of windows
	 */
	public void updateNetwork(CySwingAppAdapter adapter, CyLayoutAlgorithm algorithm, List<Shape2D> windows)
			throws InterruptedException {
		controls.algorithm = algorithm;
		filteredNetwork.doSearch(tree, adapter, controls, windows);
	}

	public int getTotalSize() {
		return totalSize;
	}

	private void initGeneList() {
		int i = 0;
		for (Gene g : genes) {
			int a = (int) ((i + 1) * (360d / genes.size()));
			i++;
			Color color = Color.getHSBColor(((a) % 360) / 360f, 0.9f, 1);
			g.setColor(color);
			g.generateArrays();
		}
	}

	public Point2D getMaxSize() {
		List<Transcript> list = tree.range(new double[] { -1 * Double.MAX_VALUE, -1 * Double.MAX_VALUE },
				new double[] { Double.MAX_VALUE, Double.MAX_VALUE });

		double maxX = -1 * Double.MAX_VALUE, maxY = -1 * Double.MAX_VALUE;
		for (Transcript t : list) {
			if (t.getX() > maxX)
				maxX = t.getX();
			if (t.getY() > maxY)
				maxY = t.getY();
		}
		return new Point2D((float) maxX, (float) maxY);
	}
	
	public Point2D getMinSize() {
		List<Transcript> list = tree.range(new double[] { -1 * Double.MAX_VALUE, -1 * Double.MAX_VALUE },
				new double[] { Double.MAX_VALUE, Double.MAX_VALUE });

		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
		for (Transcript t : list) {
			if (t.getX() < minX)
				minX = t.getX();
			if (t.getY() < minY)
				minY = t.getY();
		}
		return new Point2D((float) minX, (float) minY);
	}


	/**
	 * Get the nearest transcript to this point;
	 * 
	 * @param point
	 * @return
	 */
	public Transcript getNearest(double[] point) {
		Transcript t = null;
		try {
			t = tree.nearest(point);
		} catch (KeySizeException e) {
			e.printStackTrace();
		}
		return t;
	}

	public List<Transcript> getAllWithin(double[] point, double distance) {
		try {
			return tree.nearestEuclidean(point, distance);
		} catch (KeySizeException e) {
			e.printStackTrace();
			return new ArrayList<Transcript>();
		}
	}

	private void switchNodeRow(CyRow row) {
		String name = row.get("name", String.class);
		int index = quickIndex.get(name);
		genes.get(index).setEnabled(row.get("selected", Boolean.class));
		if(row.get("selected", Boolean.class)) {
			panel.displayAllOff();
		}
	}

	private void switchEdgeNodeRow(CyRow row, boolean enabled) {
		String name = row.get("name", String.class);
		int index = quickIndex.get(name);
		genes.get(index).setEnabled(enabled);
		if(enabled) {
			panel.displayAllOff();
		}
	}

	/***
	 * Finds network elements that have been selected
	 */
	public void selectionEvent(RowsSetEvent rse) {

		for (int i = 0; i < controls.model.size(); i++) {
			ListedNetwork ln = (ListedNetwork) controls.model.getElementAt(i);
			if (rse.getSource() == ln.getNetwork().getDefaultNodeTable()) {
				for (CyRow row : rse.getSource().getAllRows()) {
					switchNodeRow(row);
				}
			} else if (rse.getSource() == ln.getNetwork().getDefaultEdgeTable()) {
				HashSet<CyNode> enabledNodes = new HashSet<>();
				for (CyRow row : rse.getSource().getAllRows()) {
					boolean enabled = row.get("selected", Boolean.class);
					CyEdge edge = ln.getNetwork().getEdge(row.get("SUID", Long.class));
					CyNode source = edge.getSource();
					CyRow sourceRow = ln.getNetwork().getRow(source);
					CyNode target = edge.getTarget();
					CyRow targetRow = ln.getNetwork().getRow(target);

					if (enabled || !enabledNodes.contains(source)) {
						enabledNodes.add(source);
						switchEdgeNodeRow(sourceRow, enabled);
					}
					if (enabled || !enabledNodes.contains(target)) {
						enabledNodes.add(target);
						switchEdgeNodeRow(targetRow, enabled);
					}

				}
			}

		}
	}

	public List<Gene> getGenes() {
		return genes;
	}

	public Rectangle2D.Double getDimensions() {
		return rectangle;
	}

	// Take the control state container and save for this dataset
	public void setControls(ControlSet controls) {
		this.controls = controls;
	}

	// Returns the previously set control state for this dataset
	public ControlSet getControls() {
		return controls;
	}

	public SelectionPanel getPanel() {
		return panel;
	}

	public void shutDown() {
		panel.getGLPanel().shutDown();
	}

	@Override
	public String toString() {
		return filename;
	}
	
	public void setHighlightColour(Color colour) {
		filteredNetwork.setHighlightColour(colour);
	}

	public void setHighlight(boolean highlight) {
		filteredNetwork.setHighlight(highlight);
	}

	public void setMaxZ(double maxZ) {
		filteredNetwork.updateZ(maxZ);
	}

	public VisualStyle getStyle() {
		return filteredNetwork.getStyle();
	}
}