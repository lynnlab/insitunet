package org.cytoscape.insitunet.internal.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 * Utility methods for parsing input.
 * 
 * @author John Salamon
 */
public class ParseUtil {

	public static BufferedImage getImageResource(String path) {

		BufferedImage bimg;
		try {
			bimg = ImageIO.read(ParseUtil.class.getResourceAsStream(path));
		} catch (IOException | NullPointerException e) {
			JOptionPane.showMessageDialog(null, "The image at: " + path + " could not be loaded", "Warning!",
					JOptionPane.WARNING_MESSAGE);
			return null;
		}
		return bimg;
	}

	public static BufferedImage getImageFile(String path) {

		File input = new File(path);
		BufferedImage bimg;
		try {
			bimg = ImageIO.read(input);
		} catch (IOException | NullPointerException e) {
			JOptionPane.showMessageDialog(null, "The image at: " + path + " could not be loaded", "Warning!",
					JOptionPane.WARNING_MESSAGE);
			return null;
		}
		return bimg;
	}

}
