package org.cytoscape.insitunet.internal;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Properties;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.insitunet.internal.panel.MainPanel;
import org.cytoscape.insitunet.internal.panel.SelectionPanel;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.property.CyProperty;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedListener;
import org.cytoscape.view.model.events.ViewChangedListener;
import org.cytoscape.work.TaskIterator;
import org.osgi.framework.BundleContext;

/**
 * Activation and session control class. It also provides other classes with
 * access to Cytoscape APIs.
 *
 * @author John Salamon
 */
public class InsituNetActivator extends AbstractCyActivator {

	// Every unique csv imported will be assigned its own InseqSession
	private Properties properties;
	private BundleContext context;
	private MainPanel panel;
	private SessionState state;

	/**
	 * The entry point for app execution. The only immediate visible change should
	 * be a new menu option.
	 */
	@Override
	public void start(BundleContext context) throws Exception {

		this.context = context;
		panel = new MainPanel(this);
		JoglInitializer.unpackNativeLibrariesForJOGL(context); // Required for
																// using JOGL
																// within the
																// OSGi bundle

		PropertyReader propReader = new PropertyReader("insituNet", "insituNet.props");
		properties = new Properties();
		properties.setProperty("cyPropertyName", "insituNet.props");
		registerAllServices(context, propReader, properties);

		ImportAction menuAction = new ImportAction(this); // Places the import
															// option in the
															// apps menu
		registerAllServices(context, menuAction, properties);

		state = new SessionState(this);
		
		registerService(context, panel, RowsSetListener.class, properties);
		registerService(context, panel, ViewChangedListener.class, properties);
		registerService(context, state, SessionAboutToBeSavedListener.class, properties);
		registerService(context, state, SessionLoadedListener.class, properties);
	}

	/**
	 * Prompts user to import a file and initializes the session.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void initSession() throws IOException, InterruptedException {

		InsituNetImporter importer = new InsituNetImporter(this);
		if (!importer.getFile())
			return;
		if (!importer.parseFile())
			return;

		Object[] buttons = { "Continue", "Cancel" };
		int result = JOptionPane.showOptionDialog(null, importer.getMessagePanel(), "InsituNet Importer",
				JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, buttons, buttons[0]);
		if (result == 1) {
			return;
		}

		TaskIterator iterator = new TaskIterator(importer);
		getCSAA().getDialogTaskManager().execute(iterator);

	}
	
	public MainPanel getPanel() {
		return panel;
	}

	public void finalizeImport(InsituDataset dataset) {

		registerAllServices(context, panel, properties);
		panel.addDataset(dataset);

		// Switch to the InsituNet control panel.
		CytoPanel cyPanel = getCSAA().getCySwingApplication().getCytoPanel(CytoPanelName.WEST);
		cyPanel.setSelectedIndex(cyPanel.indexOfComponent(panel));
	}
	
	public void serializedImport(InsituDataset dataset) {

		registerAllServices(context, panel, properties);
		{	// Serialization procedures
			dataset.panel = new SelectionPanel(dataset, this);
		}
		panel.addSerializedDataset(dataset);

		// Switch to the InsituNet control panel.
		CytoPanel cyPanel = getCSAA().getCySwingApplication().getCytoPanel(CytoPanelName.WEST);
		cyPanel.setSelectedIndex(cyPanel.indexOfComponent(panel));
	}


	private class ImportAction extends AbstractCyAction {

		static final long serialVersionUID = 69;

		private final InsituNetActivator ia;

		public ImportAction(InsituNetActivator ia) {
			super("Import InsituNet data", ia.getCAA().getCyApplicationManager(), null, null);
			this.ia = ia;
			setPreferredMenu("Apps");
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				ia.initSession();
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Init problem: " + e.toString(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Checks wheter the file to be imported already exists in InsituNet
	 * 
	 * @return true if file exists, false otherwise
	 */
	public boolean doesImportExist(String filename) {
		return panel.datasetExists(filename);
	}

	/**
	 * Returns the CyAppAdapter convenience interface.
	 */
	public CyAppAdapter getCAA() {
		return getService(context, CyAppAdapter.class);
	}

	/**
	 * Returns the CySwingAppAdapter convenience interface.
	 */
	public CySwingAppAdapter getCSAA() {
		return getService(context, CySwingAppAdapter.class);
	}

	@SuppressWarnings("unchecked")
	public CyProperty<Properties> getProperties() {
		return getService(context, CyProperty.class, "(cyPropertyName=insituNet.props)");
	}

	@Override
	public void shutDown() {
		if (panel != null)
			panel.shutDown();
		super.shutDown();
	}
	
	public void restart() {
		//shutDown();
		panel.refresh(this);
	}
}
