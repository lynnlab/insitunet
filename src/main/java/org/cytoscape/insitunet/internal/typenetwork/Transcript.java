package org.cytoscape.insitunet.internal.typenetwork;

import java.io.Serializable;

import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.gl.Point2D;

/**
 * Defines a single in situ transcript detection. Actually can be extended to
 * multiple dimensions beyond 2D
 * 
 * @author John Salamon
 */
public class Transcript implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8184489521903187743L;
	final Double x, y; // The coordinates.
	final Integer index; // The index within the array loaded for rendering.
	final Gene gene;

	public Transcript(double x, double y, Gene gene, int index) {
		this.x = x;
		this.y = y;
		this.gene = gene;
		this.index = index;
	}

	public Point2D getPoint() {
		return new Point2D((float) getX(), (float) getY());
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public Gene getGene() {
		return gene;
	}

	public Integer getIndex() {
		return index;
	}

	@Override
	public String toString() {
		String s = "Transcript: " + x + ", " + y;
		return s;
	}
}