package org.cytoscape.insitunet.internal.typenetwork;

import java.awt.Color;
import java.awt.Paint;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;

public class ViewStyler {

	/**
	 * Generates the network style for this particular dataset
	 */
	public static VisualStyle generateStyle(InsituDataset dataset, CyAppAdapter adapter) {

		VisualStyle vs = adapter.getVisualStyleFactory().createVisualStyle("InsituNet_" + dataset);

		// Get style mapping helpers
		VisualMappingFunctionFactory continuousFactory = adapter.getVisualMappingFunctionContinuousFactory();
		VisualMappingFunctionFactory discreteFactory = adapter.getVisualMappingFunctionDiscreteFactory();
		VisualMappingFunctionFactory passthroughFactory = adapter.getVisualMappingFunctionPassthroughFactory();

		vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.DARK_GRAY);
		vs.setDefaultValue(BasicVisualLexicon.NODE_SELECTED_PAINT, new Color(255, 220, 220));
		vs.setDefaultValue(BasicVisualLexicon.EDGE_STROKE_SELECTED_PAINT, Color.YELLOW);
		vs.setDefaultValue(BasicVisualLexicon.NETWORK_BACKGROUND_PAINT, Color.BLACK);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.WHITE);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_FONT_SIZE, 20);
		vs.setDefaultValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
		vs.setDefaultValue(BasicVisualLexicon.EDGE_TRANSPARENCY, 190);
		vs.setDefaultValue(BasicVisualLexicon.NODE_LABEL_WIDTH, 50d);

		VisualLexicon lex = adapter.getRenderingEngineManager().getDefaultVisualLexicon();
		VisualProperty prop = lex.lookup(CyNode.class, "NODE_LABEL_POSITION");
		Object value = prop.parseSerializableString("SE,N,c,0.0,5.0"); // Put the north of the label on the southeast
																		// corner of the node
		vs.setDefaultValue(prop, value);

		VisualMappingFunction<String, String> ntool = passthroughFactory.createVisualMappingFunction("name",
				String.class, BasicVisualLexicon.NODE_TOOLTIP);
		vs.addVisualMappingFunction(ntool);
		VisualMappingFunction<String, String> etool = passthroughFactory.createVisualMappingFunction("name",
				String.class, BasicVisualLexicon.EDGE_TOOLTIP);
		vs.addVisualMappingFunction(etool);
		VisualMappingFunction<String, String> pMap = passthroughFactory.createVisualMappingFunction("name",
				String.class, BasicVisualLexicon.NODE_LABEL);
		vs.addVisualMappingFunction(pMap);

		// set borders so selection doesn't hide everything
		vs.setDefaultValue(BasicVisualLexicon.NODE_BORDER_WIDTH, 8d);
		VisualMappingFunction<String, Paint> border = discreteFactory.createVisualMappingFunction("name", String.class,
				BasicVisualLexicon.NODE_BORDER_PAINT);
		VisualMappingFunction<String, Paint> fill = discreteFactory.createVisualMappingFunction("name", String.class,
				BasicVisualLexicon.NODE_FILL_COLOR);

		for (Gene gene : dataset.getGenes()) {
			((DiscreteMapping<String, Paint>) border).putMapValue(gene.getName(), gene.getColor());
			((DiscreteMapping<String, Paint>) fill).putMapValue(gene.getName(), gene.getColor());
		}
		vs.addVisualMappingFunction(border);
		vs.addVisualMappingFunction(fill);

		// Set a continuous mapping for the abundance proportion
		VisualMappingFunction<Double, Double> sizeMap = continuousFactory.createVisualMappingFunction("proportion",
				Double.class, BasicVisualLexicon.NODE_SIZE);
		((ContinuousMapping<Double, Double>) sizeMap).addPoint(0d, new BoundaryRangeValues<Double>(30d, 30d, 30d));
		((ContinuousMapping<Double, Double>) sizeMap).addPoint(1d, new BoundaryRangeValues<Double>(200d, 200d, 200d));
		vs.addVisualMappingFunction(sizeMap);

		// Set colours for edges
		// BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT
		// BasicVisualLexicon.EDGE_TRANSPARENCY
		// BasicVisualLexicon.EDGE_WIDTH

		// Blue for less, red for more
		VisualMappingFunction<String, Paint> edgeColour = discreteFactory.createVisualMappingFunction("interaction",
				String.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
		((DiscreteMapping<String, Paint>) edgeColour).putMapValue("less", Color.BLUE);
		((DiscreteMapping<String, Paint>) edgeColour).putMapValue("more", Color.GREEN);
		vs.addVisualMappingFunction(edgeColour);

		VisualMappingFunction<Double, Double> edgeWidth = continuousFactory.createVisualMappingFunction("z_score",
				Double.class, BasicVisualLexicon.EDGE_WIDTH);

		((ContinuousMapping<Double, Double>) edgeWidth).addPoint(0d, new BoundaryRangeValues<Double>(1d, 1d, 1d));
		((ContinuousMapping<Double, Double>) edgeWidth).addPoint(20d, new BoundaryRangeValues<Double>(15d, 15d, 15d));

		vs.addVisualMappingFunction(edgeWidth);

		/*
		 * // Less significant edge = more transparent
		 * VisualMappingFunction<Double,Integer> edgeTransparency =
		 * continuousFactory.createVisualMappingFunction("pvalue", Double.class,
		 * BasicVisualLexicon.EDGE_TRANSPARENCY);
		 * ((ContinuousMapping<Double,Integer>)edgeTransparency).addPoint(0.0,new
		 * BoundaryRangeValues<Integer>(200, 200, 200));
		 * ((ContinuousMapping<Double,Integer>)edgeTransparency).addPoint(0.0001,new
		 * BoundaryRangeValues<Integer>(10, 10, 10));
		 * vs.addVisualMappingFunction(edgeTransparency);
		 */

		adapter.getVisualMappingManager().addVisualStyle(vs);

		return (vs);
	}

	public static VisualMappingFunction<String, Paint> getEdgeColourFunction(CyAppAdapter adapter) {

		VisualMappingFunctionFactory discreteFactory = adapter.getVisualMappingFunctionDiscreteFactory();

		VisualMappingFunction<String, Paint> edgeColour = discreteFactory.createVisualMappingFunction("interaction",
				String.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
		((DiscreteMapping<String, Paint>) edgeColour).putMapValue("less", Color.BLUE);
		((DiscreteMapping<String, Paint>) edgeColour).putMapValue("more", Color.GREEN);

		return edgeColour;
	}

	public static VisualMappingFunction<Boolean, Paint> getEdgeUniqueFunction(CyAppAdapter adapter, Color colour) {

		VisualMappingFunctionFactory discreteFactory = adapter.getVisualMappingFunctionDiscreteFactory();

		VisualMappingFunction<Boolean, Paint> edgeColour = discreteFactory.createVisualMappingFunction("unique",
				Boolean.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
		((DiscreteMapping<Boolean, Paint>) edgeColour).putMapValue(false, Color.GRAY);
		((DiscreteMapping<Boolean, Paint>) edgeColour).putMapValue(true, colour);

		return edgeColour;
	}

}
