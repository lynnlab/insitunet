package org.cytoscape.insitunet.internal.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.InsituNetActivator;
import org.cytoscape.insitunet.internal.gl.Point2D;
import org.cytoscape.insitunet.internal.gl.Shape2D;
import org.cytoscape.insitunet.internal.typenetwork.ListedNetwork;
import org.cytoscape.insitunet.internal.util.LayoutHelper;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.util.swing.DropDownMenuButton;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.model.events.ViewChangeRecord;
import org.cytoscape.view.model.events.ViewChangedEvent;
import org.cytoscape.view.model.events.ViewChangedListener;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.TaskIterator;

/*
 * The main InsituNet panel, serves as the hub of management of datasets and associated objects.
 */	
public class MainPanel extends JPanel
		implements CytoPanelComponent, ItemListener, RowsSetListener, ViewChangedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8488151593982967096L;
	private DefaultComboBoxModel<InsituDataset> datasets = new DefaultComboBoxModel<InsituDataset>();
	private JComboBox<InsituDataset> datasetSelector = new JComboBox<InsituDataset>(datasets);
	private InsituDataset currentDataset;

	InsituNetActivator activator;
	ControlPanel controlPanel;
	EmbeddedPanel embedded;
	public boolean enabled = true;

	final JList<ListedNetwork> list;
	public final DefaultListModel<ListedNetwork> listModel;
	public Map<CyNode, Set<View<CyNode>>> viewsForNode;
	JComboBox<CyLayoutAlgorithm> layoutSelector;
	LayoutHelper helper = new LayoutHelper();

	public CyNetwork unionNetwork;
	public CyNetworkView unionView;
	
	public void refresh(InsituNetActivator activator) {
		this.activator = activator;
		datasets.removeAllElements();
		listModel.removeAllElements();		
	}
	
	private class ColourButton extends JButton {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5083720406340954353L;
		public Color colour;
		
		public ColourButton(Color colour) {
			super();
			this.colour = colour;
			setBackground(colour);
			setBorderPainted(false);
			setOpaque(true);
			setPreferredSize(new Dimension(30, 20));
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
				    Color c = JColorChooser.showDialog(null, "Choose unique edge colour", colour);
				    if (c != null) setColour(c);
				}
			});
		}
		
		public void setColour(Color colour) {
			setBackground(colour);
			this.colour = colour;	
			getCurrent().setHighlightColour(colour);
			//TODO: Make it change the unique edge thing
		}

	}
	
	
	public RehydrationPack getRehydrationPack() {
		return new RehydrationPack(unionNetwork);
	}


	public MainPanel(InsituNetActivator activator) {

		this.activator = activator;
		embedded = new EmbeddedPanel(activator.getCSAA().getCySwingApplication().getJFrame());

		controlPanel = new ControlPanel(activator.getCAA(), this);

		setPreferredSize(new Dimension(400, 400));
		setLayout(new BorderLayout());

		datasetSelector.addItemListener(this);
		DropDownMenuButton newNet = new DropDownMenuButton(new MenuAction("Menu", this));

		JPanel upperPanel = new JPanel();
		upperPanel.setLayout(new GridBagLayout());

		upperPanel.add(datasetSelector, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(4, 4, 4, 0), 1, 1));
		upperPanel.add(newNet, new GridBagConstraints(1, 0, 1, 1, 0.1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(4, 0, 4, 4), 1, 1));
		upperPanel.add(controlPanel, new GridBagConstraints(0, 2, 2, 1, 1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(4, 4, 4, 0), 1, 1));

		list = new JList<>(new DefaultListModel<>());
		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if (list.getModel().getSize() > 0 && enabled) {
					ListedNetwork ln = (ListedNetwork) list.getSelectedValue();
					if (ln == null || ln.getView() == null)
						return;
					activator.getCAA().getCyApplicationManager().setCurrentNetworkView(ln.getView());
					ln.getDataset().getPanel().getGLPanel().setShapeAs(ln.getShape());
					activator.getCAA().getVisualMappingManager().setVisualStyle(ln.getDataset().getStyle(),
							ln.getView());

				}
			}
		});
		listModel = (DefaultListModel<ListedNetwork>) list.getModel();
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setMinimumSize(list.getPreferredScrollableViewportSize());

		layoutSelector = new JComboBox<>(
				activator.getCAA().getCyLayoutAlgorithmManager().getAllLayouts().toArray(new CyLayoutAlgorithm[0]));
		layoutSelector.setSelectedItem(activator.getCAA().getCyLayoutAlgorithmManager().getLayout("force-directed"));

		JButton syncLayout = new JButton("Synchronize");
		syncLayout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateSyncedNetworks();
				performSync();
			}

		});

		JCheckBox highlight = new JCheckBox("Use unique edge highlighting");
		highlight.setSelected(false);
		highlight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < datasets.getSize(); i++) {
					datasets.getElementAt(i).setHighlight(highlight.isSelected());
				}
			}

		});
		
		ColourButton cb = new ColourButton(Color.RED);
		JPanel highlightPanel = new JPanel();
		highlightPanel.add(highlight, BorderLayout.LINE_START);
		JPanel durr = new JPanel();
		durr.add(new JLabel("Colour:"), BorderLayout.LINE_START);
		durr.add(cb, BorderLayout.LINE_START);
		highlightPanel.add(durr, BorderLayout.LINE_START);

		
		JButton remove = new JButton("Remove selected from sync");
		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (ListedNetwork ln : list.getSelectedValuesList()) {
					listModel.removeElement(ln);
				}
				updateSyncedNetworks();
			}

		});

		JPanel syncPanel = new JPanel();
		syncPanel.setLayout(new GridBagLayout());
		syncPanel.add(helper.makePanel("Sync list", listScroller, remove, highlightPanel, layoutSelector, syncLayout),
				helper.makeConstraints());
		syncPanel.add(new JSeparator(), helper.makeConstraints());
		upperPanel.add(syncPanel, new GridBagConstraints(0, 1, 2, 1, 1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(4, 4, 4, 0), 1, 1));

		add(upperPanel, BorderLayout.NORTH);
		add(embedded, BorderLayout.CENTER);
	}
	
	public void addSerializedDataset(InsituDataset dataset) {
		// do some kind of deserialization logic?
		datasets.addElement(dataset);
		datasets.setSelectedItem(dataset);
		embedded.embedCurrent();
	}

	public void addDataset(InsituDataset dataset) {

		dataset.setControls(controlPanel.newState(dataset.getDimensions().getWidth(), dataset.getDimensions().getHeight())); // Set the new dataset control state to copy of current state
		datasets.addElement(dataset);
		datasets.setSelectedItem(dataset);
		embedded.embedCurrent();
	}

	public void deleteCurrent() {
		currentDataset.shutDown();
		datasets.removeElement(currentDataset);
	}

	public ArrayList<InsituDataset> getDatasets() {
		ArrayList<InsituDataset> list = new ArrayList<>();
		for (int i = 0; i < datasets.getSize(); i++) {
			list.add(datasets.getElementAt(i));
		}
		return list;
	}
	
	public InsituDataset getCurrent() {
		return currentDataset;
	}

	public void generateNetwork() {
		currentDataset.setControls(controlPanel.saveState()); // Save controlset to dataset
		try {
			if (currentDataset.getControls().useManual) {
				currentDataset.updateNetwork(activator.getCSAA(), (CyLayoutAlgorithm) layoutSelector.getSelectedItem()); // Search
																															// in
																															// the
																															// kdTree
			} else {

				// Sliding window setup logic

				if (currentDataset.getControls().x < 2 && currentDataset.getControls().y < 2) {
					int yesno = JOptionPane.showConfirmDialog(
						    null,
						    "You have selected the sliding window option with a 1x1 grid, which will create only one window over the entire dataset.\n"
						    + "You can fix this by using manual selection or adjusting \"Autoslider dimensions\" under Region Selection. Continue anyway?",
						    "Sliding Window Setup",
						    JOptionPane.YES_NO_OPTION);
					if(yesno == 1) return;
				}
				
				Rectangle2D.Double r = currentDataset.getDimensions();
				Point2D total = new Point2D(r.getWidth(), r.getHeight());

				Point2D windowSize = new Point2D(total.x / currentDataset.getControls().x,
						total.y / currentDataset.getControls().y);

				int numHorizontal = (int) Math.ceil(total.x / windowSize.x);
				int numVertical = (int) Math.ceil(total.y / windowSize.y);

				float remHoz = (float) numHorizontal * windowSize.x - total.x;
				int subHoz = (int) (remHoz / (float) numHorizontal);

				float remVer = (float) numVertical * windowSize.y - total.y;
				int subVer = (int) (remVer / (float) numVertical);

				List<Shape2D> windows = new ArrayList<>();
				for (int h = 0; h < numVertical; h++) {
					for (int w = 0; w < numHorizontal; w++) {
						Shape2D rec = new Shape2D();
						float xOver = (float) (windowSize.x * currentDataset.getControls().overlap / 100d);
						float yOver = (float) (windowSize.y * currentDataset.getControls().overlap / 100d);

						rec.push(new Point2D((w * windowSize.x - w * subHoz) - xOver + r.x,
								(h * windowSize.y - h * subVer) - yOver + r.y));
						rec.push(new Point2D((w * windowSize.x - w * subHoz) + windowSize.x + xOver + r.x,
								(h * windowSize.y - h * subVer) - yOver + r.y));
						rec.push(new Point2D((w * windowSize.x - w * subHoz) + windowSize.x + xOver + r.x,
								((h * windowSize.y - h * subVer) + windowSize.y) + yOver + r.y));
						rec.push(new Point2D((w * windowSize.x - w * subHoz) - xOver + r.x,
								((h * windowSize.y - h * subVer) + windowSize.y) + yOver + r.y));
						rec.push(new Point2D((w * windowSize.x - w * subHoz) - xOver + r.x,
								(h * windowSize.y - h * subVer) - yOver + r.y));
						windows.add(rec);
					}
				}

				currentDataset.updateNetwork(activator.getCSAA(), (CyLayoutAlgorithm) layoutSelector.getSelectedItem(),
						windows);

			}
			// instead of this, we should add all available network views if they aren't
			// there already
			// maybe we could do checking here on whether they still exist
			// currentDataset.getControls().model.addElement(currentDataset.getBaseNetwork().getBaseView());
		} catch (InterruptedException e) {
			System.out.println("Generation was interrupted");
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		/*
		 * We listen to state changes in the dataset list. When a dataset is deselected
		 * the state of the GUI controls is backed up. When reselected the state is
		 * reapplied.
		 */
		if (e.getStateChange() == ItemEvent.DESELECTED) {
			currentDataset.setControls(controlPanel.saveState());
		} else if (e.getStateChange() == ItemEvent.SELECTED) {
			currentDataset = (InsituDataset) datasetSelector.getSelectedItem();
			controlPanel.loadState(currentDataset.getControls());
			embedded.setCurrentChild(currentDataset.getPanel());
		}
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	@Override
	public String getTitle() {
		return "InsituNet";
	}

	public boolean datasetExists(String filename) {
		for (int i = 0; i < datasets.getSize(); i++) {
			if (datasets.getElementAt(i).toString().equals(filename)) {
				return true;
			}
		}
		return false;
	}

	public void shutDown() {
		for (int i = 0; i < datasets.getSize(); i++) {
			datasets.getElementAt(i).shutDown();
		}
		datasets.removeAllElements();
	}

	@Override
	public void handleEvent(RowsSetEvent rse) {

		InsituDataset current = (InsituDataset) datasets.getSelectedItem();
		current.selectionEvent(rse);
		current.getPanel().getGLPanel().go();
	}
	
	public int getLNPosition(ListedNetwork ln) {
		int i = 0;
		while (i < listModel.size()) {
			if (ln == listModel.get(i)) {
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public int getLNLength() {
		return listModel.size();
	}

	@Override
	public void handleEvent(ViewChangedEvent<?> vce) {

		//System.out.println("ViewChangedEvent!");
		CyNetworkView source = vce.getSource();

		for (ViewChangeRecord<?> record : vce.getPayloadCollection()) {

			CyNetworkView currentNetworkView = activator.getCAA().getCyApplicationManager().getCurrentNetworkView();
			if (source != currentNetworkView)
				return;

			//System.out.println("Checking if view is synced!");

			boolean currentViewIsSynced = false;
			for (int i = 0; i < listModel.size(); i++) {
				ListedNetwork ln = listModel.get(i);
				if (ln.getView() == currentNetworkView) {
					//System.out.println("It is!");
					currentViewIsSynced = true;
				}
			}
			if (!currentViewIsSynced)
				return;

			VisualProperty<?> vp = record.getVisualProperty();

			if (record.getView().getModel() instanceof CyNode) {
				CyNode node = (CyNode) record.getView().getModel();
				if (vp == BasicVisualLexicon.NODE_X_LOCATION || vp == BasicVisualLexicon.NODE_Y_LOCATION) {

					//System.out.println("It's an xy event!");

					double x = record.getView().getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
					double y = record.getView().getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);

					if (!viewsForNode.containsKey(node))
						break;
					
					//System.out.println("We've registered this node!");

					for (View<CyNode> nodeView : viewsForNode.get(node)) {
						if (currentNetworkView.getAllViews().contains(nodeView))
							continue;
						
						//System.out.println("We're setting the parallel views!");

						nodeView.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
						nodeView.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
					}
				}
			} else {

				if (vp == BasicVisualLexicon.NETWORK_SCALE_FACTOR) {
					double scale = record.getView().getVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR);
					for (int i = 0; i < listModel.size(); i++) {
						ListedNetwork ln = listModel.get(i);
						if (currentNetworkView == ln.getView())
							continue;
						ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR, scale);
					}
				} else if (vp == BasicVisualLexicon.NETWORK_CENTER_X_LOCATION
						|| vp == BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION) {

					double x = record.getView().getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION);
					double y = record.getView().getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION);

					for (int i = 0; i < listModel.size(); i++) {
						ListedNetwork ln = listModel.get(i);
						if (currentNetworkView == ln.getView())
							continue;
						ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, x);
						ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION, y);

						// doesn't seem to be needed
						// ln.getView().updateView();
						// activator.getCAA().getCyEventHelper().flushPayloadEvents();

					}

				}
			}
		}
	}

	public void addToSyncList(List<ListedNetwork> netList) {

		// First, add all new networks to the list
		for (ListedNetwork ln : netList) {
			if (!listModel.contains(ln)) {
				listModel.addElement(ln);
			}
		}
		updateSyncedNetworks();
		performSync();
	}

	
	public void updateSyncedNetworks() {
		Map<String, Set<CyNode>> nodeMap = new HashMap<>();
		Map<String, Set<View<CyNode>>> nodeViewMap = new HashMap<>();

		Map<CyNode, Set<View<CyNode>>> similarNodes = new HashMap<>(); // Now given a node, we can get all other nodes
																		// with same name, without looking it up first
		Map<String, CyNode> unionNodes = new HashMap<>();

		for (int i = 0; i < listModel.size(); i++) {
			ListedNetwork ln = listModel.get(i);
			CyNetwork n = ln.getNetwork();
			// Build the set of nodes
			for (CyNode node : ln.getNodes()) {
				String name = n.getRow(node).get(CyNetwork.NAME, String.class);
				if (nodeMap.containsKey(name)) {
					nodeMap.get(name).add(node);
					nodeViewMap.get(name).add(ln.getView().getNodeView(node));
				} else {
					Set<CyNode> set = new HashSet<>();
					set.add(node);
					nodeMap.put(name, set);
					Set<View<CyNode>> viewSet = new HashSet<>();
					viewSet.add(ln.getView().getNodeView(node));
					nodeViewMap.put(name, viewSet);
				}
				similarNodes.put(node, nodeViewMap.get(name));
			}
		}

		CyNetwork union = activator.getCAA().getCyNetworkFactory().createNetwork();
		union.getDefaultEdgeTable().createColumn("unique", Boolean.class, false); // The number of coexpression events
																					// of these two transcripts

		for (String name : nodeMap.keySet()) {
			CyNode node = union.addNode();
			CyRow row = union.getDefaultNodeTable().getRow(node.getSUID());
			row.set(CyNetwork.NAME, name);
			unionNodes.put(name, node);
			similarNodes.put(node, nodeViewMap.get(name));
		}

		for (int i = 0; i < listModel.size(); i++) {
			ListedNetwork ln = listModel.get(i);
			for (CyEdge edge : ln.getNetwork().getEdgeList()) {
				String source = ln.getNetwork().getRow(edge.getSource()).get(CyNetwork.NAME, String.class);
				String target = ln.getNetwork().getRow(edge.getTarget()).get(CyNetwork.NAME, String.class);
				CyNode sourceNode = unionNodes.get(source);
				CyNode targetNode = unionNodes.get(target);
				if (!union.containsEdge(sourceNode, targetNode)) {
					CyEdge uniqueEdge = union.addEdge(sourceNode, targetNode, false);
					CyRow row = union.getDefaultEdgeTable().getRow(uniqueEdge.getSUID());
					row.set("unique", true);
				} else {
					CyEdge notUniqueEdge = union.getConnectingEdgeList(sourceNode, targetNode, CyEdge.Type.ANY).get(0);
					CyRow row = union.getDefaultEdgeTable().getRow(notUniqueEdge.getSUID());
					row.set("unique", false);
				}
			}
		}

		// Go through again and assign uniqueness
		for (int i = 0; i < listModel.size(); i++) {
			ListedNetwork ln = listModel.get(i);
			for (CyEdge edge : ln.getNetwork().getEdgeList()) {
				String source = ln.getNetwork().getRow(edge.getSource()).get(CyNetwork.NAME, String.class);
				String target = ln.getNetwork().getRow(edge.getTarget()).get(CyNetwork.NAME, String.class);
				CyNode sourceNode = unionNodes.get(source);
				CyNode targetNode = unionNodes.get(target);
				CyEdge unionEdge = union.getConnectingEdgeList(sourceNode, targetNode, CyEdge.Type.ANY).get(0);
				CyRow unionRow = union.getDefaultEdgeTable().getRow(unionEdge.getSUID());
				boolean isUnique = unionRow.get("unique", Boolean.class);

				CyRow row = ln.getNetwork().getDefaultEdgeTable().getRow(edge.getSUID());
				row.set("unique", isUnique);
			}
		}

		viewsForNode = similarNodes;
		unionNetwork = union;

	}
	
	public void rehydrateNodeMap() {
		Map<String, Set<CyNode>> nodeMap = new HashMap<>();
		Map<String, Set<View<CyNode>>> nodeViewMap = new HashMap<>();

		Map<CyNode, Set<View<CyNode>>> similarNodes = new HashMap<>(); // Now given a node, we can get all other nodes
																		// with same name, without looking it up first
		for (int i = 0; i < listModel.size(); i++) {
			ListedNetwork ln = listModel.get(i);
			CyNetwork n = ln.getNetwork();
			// Build the set of nodes
			for (CyNode node : ln.getNodes()) {
				String name = n.getRow(node).get(CyNetwork.NAME, String.class);
				if (nodeMap.containsKey(name)) {
					nodeMap.get(name).add(node);
					nodeViewMap.get(name).add(ln.getView().getNodeView(node));
				} else {
					Set<CyNode> set = new HashSet<>();
					set.add(node);
					nodeMap.put(name, set);
					Set<View<CyNode>> viewSet = new HashSet<>();
					viewSet.add(ln.getView().getNodeView(node));
					nodeViewMap.put(name, viewSet);
				}
				similarNodes.put(node, nodeViewMap.get(name));
			}
		}
		viewsForNode = similarNodes;
	}

	/**
	 * Generate a new layout and synchronize the networks
	 */
	public void performSync() {

		if (unionNetwork == null)
			return;

		// Apply layout
		unionView = activator.getCAA().getCyNetworkViewFactory().createNetworkView(unionNetwork);
		SynchronousTaskManager<?> synTaskMan = activator.getCAA().getCyServiceRegistrar()
				.getService(SynchronousTaskManager.class);
		CyLayoutAlgorithm algorithm = (CyLayoutAlgorithm) layoutSelector.getSelectedItem();
		TaskIterator iterator = algorithm.createTaskIterator(unionView, algorithm.createLayoutContext(),
				CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		synTaskMan.execute(iterator);
		unionView.updateView();
		activator.getCAA().getCyEventHelper().flushPayloadEvents();

		// Find union node locations
		for (CyNode node : unionNetwork.getNodeList()) {

			double x = unionView.getNodeView(node).getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
			double y = unionView.getNodeView(node).getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);

			for (View<CyNode> nodeView : viewsForNode.get(node)) {
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
				nodeView.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
			}
		}

		double x = unionView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION);
		double y = unionView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION);

		double scale = unionView.getVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR);
		for (int i = 0; i < listModel.size(); i++) {
			ListedNetwork ln = listModel.get(i);
			ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR, scale);
			ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, x);
			ln.getView().setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION, y);
		}

	}

	public void setMaxZ(double maxZ) {
		currentDataset.setMaxZ(maxZ);

	}


}
