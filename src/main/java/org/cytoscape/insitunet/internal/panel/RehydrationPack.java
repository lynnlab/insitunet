package org.cytoscape.insitunet.internal.panel;

import java.io.Serializable;
import org.cytoscape.model.CyNetwork;

public class RehydrationPack implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -582089252019666708L;
	public int[][] positions;
	public long networkSUID;
	
	public RehydrationPack(CyNetwork unionNetwork) {
		if (unionNetwork != null) {
			this.networkSUID = unionNetwork.getSUID();
		}
	}
}