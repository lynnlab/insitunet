package org.cytoscape.insitunet.internal.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cytoscape.insitunet.internal.panel.SelectionPanel.PositionListener;

/*
 * A panel that allows another component to be embedded within itself,
 * And provides a mechanism to un-pin into a separate frame. This is the lower section of the MainPanel.
 *
 * @author John Salamon
 * 
 */
class EmbeddedPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1998L;

	private SelectionPanel child;
	final StatusBar bar = new StatusBar();
	final JFrame windowFrame;

	final Dimension buttonSize = new Dimension(22, 22);
	final ImageIcon pin = SelectionPanel.getIconResource("pin", buttonSize);
	final ImageIcon pop = SelectionPanel.getIconResource("pop", buttonSize);

	public EmbeddedPanel(JFrame windowFrame) {

		this.windowFrame = windowFrame;
		setLayout(new BorderLayout());
		bar.addPinButtonListener(this);
		add(bar, BorderLayout.SOUTH);
	}

	/*
	 * If the current dataset changes, pass its selection panel here. The method
	 * figures out if it is in a frame or not and sets the statuspanel accordingly.
	 */
	void setCurrentChild(SelectionPanel child) {

		if (this.child != null)
			this.remove(this.child);
		this.child = child;

		if (child.getParent() != null) {
			bar.getPinButton().setIcon(pin);
		} else {
			embedCurrent();
			bar.getPinButton().setIcon(pop);
		}
		repaint();
	}

	public void embedCurrent() {
		add(child, BorderLayout.CENTER);
		bar.getPinButton().setIcon(pop);
		bar.setStatusText(child.getLabel());
		child.setPositionListener(bar);
		repaint();
	}

	public void takeBack(SelectionPanel child) {
		if (child == this.child) {
			// this is the currently active dataset, we'll display it
			embedCurrent();
		} else {
			// we aren't viewing this at the moment, ignore it
		}
	}

	/*
	 * Pin/unpin the child panel from the embedded panel.
	 */
	public void actionPerformed(ActionEvent e) {
		if (child.getParent() == this) {
			// The current child is embedded, cast it out
			new SeparateFrame(child);
			bar.getPinButton().setIcon(pin);
			bar.setStatusText("");
			bar.setPositionText("Panel has been un-pinned");
			repaint();

		} else {
			// The child is not here, call it back
			embedCurrent();
		}
	}

	/*
	 * This is the frame the selectionpanel is placed into when un-pinned.
	 */
	class SeparateFrame extends JFrame implements ActionListener {
		static final long serialVersionUID = 4324324l;

		final SelectionPanel child;
		final StatusBar bar = new StatusBar();

		SeparateFrame(SelectionPanel child) {
			super(child.getLabel() + " - InsituNet");
			this.child = child;
			bar.getPinButton().setIcon(pin);
			bar.addPinButtonListener(this);
			bar.setStatusText(child.getLabel());
			child.setPositionListener(bar);
			this.setMinimumSize(new Dimension(300, 240));
			setLayout(new BorderLayout());
			add(child);
			add(bar, BorderLayout.SOUTH);
			pack();
			setLocationRelativeTo(windowFrame);
			setVisible(true);

			this.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					EmbeddedPanel.this.takeBack(child);
					close();
				}
			});

			this.getContentPane().addContainerListener(new ContainerListener() {
				@Override
				public void componentAdded(ContainerEvent e) {
				};

				@Override
				public void componentRemoved(ContainerEvent e) {
					close();
				}
			});

		}

		@Override
		public void actionPerformed(ActionEvent e) {
			EmbeddedPanel.this.takeBack(child);
			close();
		}

		public void close() {
			this.remove(child);
			this.setVisible(false);
			this.dispose();
		}
	}

	/*
	 * Just a status bar, contains a button for pinning/unpinning and displays
	 * generic info.
	 */
	class StatusBar extends JPanel implements PositionListener {

		private static final long serialVersionUID = -8686368833252766135L;
		final JButton pinButton = new JButton(pop);
		final JLabel statusLabel = new JLabel();
		final JLabel positionLabel = new JLabel();

		public StatusBar() {
			setPreferredSize(new Dimension(getWidth(), 24));
			setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
			pinButton.setToolTipText("Pin/unpin this view");
			add(pinButton);
			add(Box.createRigidArea(new Dimension(5, 0)));
			add(statusLabel);
			add(Box.createHorizontalGlue());
			add(positionLabel);
			add(Box.createRigidArea(new Dimension(5, 0)));
		}

		public void setStatusText(String text) {
			statusLabel.setText(text);
		}

		public void setPositionText(String text) {
			positionLabel.setText(text);
		}

		public void addPinButtonListener(ActionListener a) {
			pinButton.addActionListener(a);
		}

		public JButton getPinButton() {
			return pinButton;
		}

		@Override
		public void updateRightLabel(String string) {
			setPositionText(string);
		}
	}
}