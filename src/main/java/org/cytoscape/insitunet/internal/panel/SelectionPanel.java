package org.cytoscape.insitunet.internal.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.spi.IIORegistry;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.InsituNetActivator;
import org.cytoscape.insitunet.internal.gl.GLPanel;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;

import com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReaderSpi;

/**
 * A panel containing the zoomable image as well as controls. It will start
 * embedded in MainPanel, but can become a seperate window. I guess we're going
 * to start storing state in here... namely the current shape... etc
 * 
 * @author John Salamon
 */
public class SelectionPanel extends JPanel {

	public enum SelectionState {
		RECTANGLE, POLYGON
	}

	private static final long serialVersionUID = -3656880368971065116L;

	final JPanel plotControls = new JPanel();
	final GLPanel glPanel;
	private JButton colourPicker;
	final String label;
	final Dimension buttonSize = new Dimension(40, 30);
	final Dimension iconSize = new Dimension(30, 30);
	StylePanel stylePanel;
	IconToggleButton showAll;

	Font font;

	public static ImageIcon getIconResource(String resource, Dimension size) {
		java.net.URL url = SelectionPanel.class.getResource("/icons/" + resource + ".png");
		if (url == null) {
			System.out.println("Couldn't find " + resource);
			return new ImageIcon();
		} else {
			ImageIcon icon = new ImageIcon(url);
			Image img = icon.getImage();
			Image scaled = img.getScaledInstance(size.width, size.height, java.awt.Image.SCALE_SMOOTH);
			return new ImageIcon(scaled);
		}
	}

	class IconButton extends JButton {
		/**
		 * Shortcut class to create a non-toggle button with an image from resource
		 */
		private static final long serialVersionUID = -4977644547894989915L;

		public IconButton(String resource) {
			super(getIconResource(resource, iconSize));
			setPreferredSize(buttonSize);
		}
	}

	class IconToggleButton extends JToggleButton {
		/**
		 * Shortcut class for creating a toggle button with image from resource
		 */
		private static final long serialVersionUID = 8457158077139424817L;

		public IconToggleButton(String resource) {
			super(getIconResource(resource, iconSize));
			setPreferredSize(buttonSize);
		}
	}

	class MenuIconButton extends JButton {

		/**
		 * Shortcut class for creating a popup menu button with image from resource
		 */
		private static final long serialVersionUID = -5772015910715905489L;
		final JPopupMenu popup = new JPopupMenu();

		MenuIconButton(String resource) {
			super(getIconResource(resource, iconSize));
			setPreferredSize(buttonSize);

			addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					Component c = e.getComponent();
					popup.show(c, 0, c.getHeight());
					popup.repaint();
				}
			});
		}

		public void add(JMenuItem item) {
			popup.add(item);
		}

	}

	public String getLabel() {
		return label;
	}
	
	public void displayAllOff() {
		this.showAll.setSelected(false);
		glPanel.setShowAll(showAll.isSelected());
	}

	public SelectionPanel(InsituDataset dataset, InsituNetActivator activator) {

		setLayout(new BorderLayout());
		label = dataset.toString();
		JPopupMenu.setDefaultLightWeightPopupEnabled(false); // not sure if this even works,
		// but it's trying to stop the menus from being hidden behind the GLPanel

		plotControls.setPreferredSize(new Dimension(getWidth(), 30));
		BoxLayout layout = new BoxLayout(plotControls, BoxLayout.X_AXIS);
		plotControls.setLayout(layout);

		String prop = activator.getProperties().getProperties().getProperty("insituNet.useAntiAliasing");
		boolean useAA = "true".equalsIgnoreCase(prop);

		glPanel = new GLPanel(dataset, useAA);
		add(plotControls, BorderLayout.NORTH);
		add(glPanel, BorderLayout.CENTER);

		MenuIconButton imageMenu = new MenuIconButton("file");
		imageMenu.setToolTipText("Select and adjust tissue image");
		imageMenu.add(new JMenuItem(new AbstractAction("Load new image") {
			/**
			 * Load an image to back the points.
			 */
			private static final long serialVersionUID = -5275946186746142861L;

			public void actionPerformed(ActionEvent e) {
				String path = openFilePrompt(null);
				if (path != null) {
					try {
						// Enable TIFF support
						IIORegistry reg = IIORegistry.getDefaultInstance();
						reg.registerServiceProvider(new TIFFImageReaderSpi());

						BufferedImage image = ImageIO.read(new File(path));
						glPanel.setImage(image);
					} catch (IOException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, "Failed to parse image", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}

		}));
		imageMenu.add(new JMenuItem(new AbstractAction("Rescale image") {
			/**
			 * Allow scaling of the imported image relative to points.
			 */
			private static final long serialVersionUID = -5820785041428400192L;

			public void actionPerformed(ActionEvent e) {
				Float f = getImageScale();
				if (f != null) {
					glPanel.setImageScale(f);
				}
			}
		}));
		plotControls.add(imageMenu);

		showAll = new IconToggleButton("eye");
		showAll.setToolTipText("Toggle show all transcripts");
		showAll.setSelected(true);
		plotControls.add(showAll);
		showAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				glPanel.setShowAll(showAll.isSelected());
			}
		});

		IconToggleButton rectangleSelect = new IconToggleButton("rectangle");
		rectangleSelect.setToolTipText("Use the rectangular selection tool");
		rectangleSelect.setSelected(true);
		plotControls.add(rectangleSelect);
		rectangleSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				glPanel.setSelectionState(SelectionState.RECTANGLE);
			}
		});

		IconToggleButton polygonSelect = new IconToggleButton("polygon");
		polygonSelect.setToolTipText("Use the polygonal/freeform selection tool");
		plotControls.add(polygonSelect);
		polygonSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				glPanel.setSelectionState(SelectionState.POLYGON);
			}
		});

		ButtonGroup areaGroup = new ButtonGroup();
		areaGroup.add(rectangleSelect);
		areaGroup.add(polygonSelect);

		IconButton center = new IconButton("center");
		center.setToolTipText("Re-center the view");
		plotControls.add(center);
		center.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				glPanel.center();
			}
		});

		IconButton colour = new IconButton("colour");
		colour.setToolTipText("Customise symbol colour and shape");
		colour.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (stylePanel == null) {
					stylePanel = new StylePanel(glPanel, dataset,
							activator.getCSAA().getCySwingApplication().getJFrame());
				}
				stylePanel.show(glPanel.getSelected());
			}
		});
		plotControls.add(colour);

		IconToggleButton info = new IconToggleButton("info");
		info.setSelected(true);
		info.setToolTipText("Toggle legend and scale display");
		info.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				glPanel.showGUI(info.isSelected());
			}
		});
		plotControls.add(info);

		MenuIconButton export = new MenuIconButton("export");
		export.setToolTipText("Export this view as an image");
		export.add(new JMenuItem(new AbstractAction("Export current view as png...") {
			/**
			 * Allows export of the GL Window view as a PNG
			 */
			private static final long serialVersionUID = 5399757354537488535L;

			public void actionPerformed(ActionEvent e) {
				String path = saveFilePrompt(null);
				if (path != null) {
					glPanel.outputToFile(path);
				}
			}
		}));
		export.add(new JMenuItem(new AbstractAction("Export with custom resolution...") {
			/**
			 * Export the GL Window view as if it were at the specified resolution
			 */
			private static final long serialVersionUID = -2597791793169397754L;

			public void actionPerformed(ActionEvent e) {
				int[] res = getResolution();
				if (res != null) {
					String path = saveFilePrompt(null);
					if (path != null) {
						glPanel.outputToFile(path, res[0], res[1]);
					}
				}
			}
		}));
		plotControls.add(export);

	}

	public Float getImageScale() {

		JFormattedTextField field = new JFormattedTextField(1f);
		field.setValue(glPanel.getImageScale());
		field.setColumns(3);

		JPanel message = new JPanel();
		message.add(new JLabel("New image scaling factor:"));
		message.add(field);

		int result = JOptionPane.showConfirmDialog(null, message, "Adjust image scale", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			try {
				float scale = (float) field.getValue();
				return scale;
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error parsing input", "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		} else
			return null;
	}

	public int[] getResolution() {

		JFormattedTextField widthField = new JFormattedTextField(4);
		widthField.setValue(glPanel.getWidth());
		widthField.setColumns(5);
		JFormattedTextField heightField = new JFormattedTextField(4);
		heightField.setValue(glPanel.getHeight());
		heightField.setColumns(5);

		JPanel message = new JPanel();
		message.add(widthField);
		message.add(new JLabel("x"));
		message.add(heightField);

		int result = JOptionPane.showConfirmDialog(null, message, "Set output resolution", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			try {
				int width = (int) widthField.getValue();
				int height = (int) heightField.getValue();
				if (width > glPanel.getMaxBufSize() || height > glPanel.getMaxBufSize()) {
					JOptionPane.showMessageDialog(null,
							"Dimensions too large: Maximum supported size: " + glPanel.getMaxBufSize() + "x"
									+ glPanel.getMaxBufSize(),
							"GL_MAX_RENDERBUFFER is too small", JOptionPane.ERROR_MESSAGE);
					return null;
				}
				return new int[] { width, height };
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Error parsing input", "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		} else
			return null;
	}

	public String openFilePrompt(Component parent) {
		JFileChooser fc = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "tif", "tiff", "jpg", "png", "gif",
				"jpeg");
		fc.setFileFilter(filter);

		if (fc.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile().getAbsolutePath();
		}
		return null;
	}

	public String saveFilePrompt(Component parent) {
		JFileChooser fc = new JFileChooser() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -8149149517437910996L;

			@Override
			public void approveSelection() {
				File f = getSelectedFile();
				if (f.exists()) {
					int result = JOptionPane.showConfirmDialog(this, "The selected file already exists, overwrite?",
							"Confirm Overwrite", JOptionPane.YES_NO_OPTION);
					switch (result) {
					case JOptionPane.YES_OPTION:
						super.approveSelection();
						return;
					case JOptionPane.NO_OPTION:
						return;
					case JOptionPane.CLOSED_OPTION:
						return;
					}
				}
				super.approveSelection();
			}

		};
		fc.setSelectedFile(new File("snapshot_" + System.currentTimeMillis() + ".png"));

		if (fc.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile().getAbsolutePath();
		}

		return null;
	}

	interface PositionListener {
		public void updateRightLabel(String string);
	}

	private PositionListener positionListener;

	void setPositionListener(PositionListener m) {
		positionListener = m;
	}

	public void signalPositionChange(float x, float y) {
		if (positionListener != null) {
			positionListener.updateRightLabel(String.format("(%g, %g)", x, y));
		}
	}

	public GLPanel getGLPanel() {
		return glPanel;
	}

	public void setSelected(Transcript t) {
		if (t == null) {
			colourPicker.setEnabled(false);
		} else {
			colourPicker.setEnabled(true);
		}

	}

}
