#version 330 core

/*
 * Very simple fragment shader, just has a single colour uniform.
 */

uniform vec4 colour;
uniform sampler2D sampler;

in vec2 tex_coord;
out vec4 out_color;

void main(void) {

	out_color = colour + texture(sampler, tex_coord);

}